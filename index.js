const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const logger = require('./lib/tools').logger
const process = require('./lib/process')

const port = 3000

app.use(bodyParser.text())

app.post('/process', (req, res) => {
  res.send(process.processInstructionMessage(req.body))
})

app.listen(port, () => logger.info(`Martian Robot app running on port ${port}`))
