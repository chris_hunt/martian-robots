# Martian Robots

## Notes
* Assume that all messages are valid
* Grid will not extend negatively
* Additional commands can be added to `lib/commands.js`

## To install
Clone repo and then run
```
npm install
```

## To run
```
node index
```

## To process robot
POST to `/process` endpoint with the instructions in the body.
Example...
```
curl -X POST \
  http://localhost:3000/process \
  -H 'content-type: text/plain' \
  -d '5 3
1 1 E
RFRFRFRF

3 2 N
FRRFLLFFRRFLL

0 3 W
LLFFFLFLFL'
```

## To run tests
```
npm test
```