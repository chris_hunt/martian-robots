/*
Position should be an object
{
  x: x-coord,
  y: y-coord,
  dir: direction facing
}
*/

const commands = {
  F: (pos) => {
    let newPos = Object.assign({}, pos)
    switch (newPos.dir) {
      case 'N':
        newPos.y = newPos.y + 1
        break
      case 'E':
        newPos.x = newPos.x + 1
        break
      case 'W':
        newPos.x = newPos.x - 1
        break
      case 'S':
        newPos.y = newPos.y - 1
        break
    }
    return newPos
  },
  L: (pos) => {
    let newPos = Object.assign({}, pos)
    switch (newPos.dir) {
      case 'N':
        newPos.dir = 'W'
        break
      case 'E':
        newPos.dir = 'N'
        break
      case 'W':
        newPos.dir = 'S'
        break
      case 'S':
        newPos.dir = 'E'
        break
    }
    return newPos
  },
  R: (pos) => {
    let newPos = Object.assign({}, pos)
    switch (pos.dir) {
      case 'N':
        newPos.dir = 'E'
        break
      case 'E':
        newPos.dir = 'S'
        break
      case 'W':
        newPos.dir = 'N'
        break
      case 'S':
        newPos.dir = 'W'
        break
    }
    return newPos
  }
}

module.exports = commands
