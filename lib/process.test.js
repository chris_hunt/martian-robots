const expect = require('chai').expect
const process = require('./process')

describe('Instruction processor', () => {
  it('should break instructions into parts', () => {
    let instructions = `5 3
1 1 E
RFRFRFRF

3 2 N
FRRFLLFFRRFLL

0 3 W
LLFFFLFLFL`

    let splitInstructions = process.breakdownInstructions(instructions)

    expect(splitInstructions).to.deep.equal({
      grid: {
        x: 5,
        y: 3
      },
      robots: [
        {
          startingPoint: {
            x: 1,
            y: 1,
            dir: 'E'
          },
          moves: ['R', 'F', 'R', 'F', 'R', 'F', 'R', 'F']
        },
        {
          startingPoint: {
            x: 3,
            y: 2,
            dir: 'N'
          },
          moves: ['F', 'R', 'R', 'F', 'L', 'L', 'F', 'F', 'R', 'R', 'F', 'L', 'L']
        },
        {
          startingPoint: {
            x: 0,
            y: 3,
            dir: 'W'
          },
          moves: ['L', 'L', 'F', 'F', 'F', 'L', 'F', 'L', 'F', 'L']
        }
      ]
    })
  })

  it('should detect if robot is on grid', () => {
    expect(
      process.isOnGrid({x: 5, y: 3}, {x: 4, y: 2, dir: 'N'})
    ).to.be.equal(true)

    expect(
      process.isOnGrid({x: 5, y: 3}, {x: 6, y: 2, dir: 'N'})
    ).to.be.equal(false)

    expect(
      process.isOnGrid({x: 5, y: 3}, {x: -1, y: 2, dir: 'N'})
    ).to.be.equal(false)

    expect(
      process.isOnGrid({x: 5, y: 3}, {x: 2, y: -1, dir: 'N'})
    ).to.be.equal(false)
  })

  it('should process moves given instructions', () => {
    let positions = process.processMoves({
      grid: {
        x: 5,
        y: 3
      },
      robots: [
        {
          startingPoint: {
            x: 1,
            y: 1,
            dir: 'E'
          },
          moves: ['R', 'F', 'R', 'F', 'R', 'F', 'R', 'F']
        }
      ]
    })
    // Check the last move
    expect(positions[0][positions[0].length - 1]).to.deep.equal({x: 1, y: 1, dir: 'E'})
  })

  it('should detect a lost robot', () => {
    let positions = process.processMoves({
      grid: {
        x: 5,
        y: 3
      },
      robots: [
        {
          startingPoint: {
            x: 1,
            y: 1,
            dir: 'E'
          },
          moves: ['R', 'F', 'R', 'F', 'R', 'F', 'R', 'F']
        },
        {
          startingPoint: {
            x: 3,
            y: 2,
            dir: 'N'
          },
          moves: ['F', 'R', 'R', 'F', 'L', 'L', 'F', 'F', 'R', 'R', 'F', 'L', 'L']
        }
      ]
    })
    // Check the last two moves - the LOST command and the position before
    expect(positions[1][positions[1].length - 2]).to.deep.equal({x: 3, y: 3, dir: 'N'})
    expect(positions[1][positions[1].length - 1]).to.be.equal('LOST')
  })

  it('should compare positions and commands with previous lost commands', () => {
    process.clearLostCommands()
    process.addLostCommand({x: 1, y: 2, dir: 'N'}, 'F')
    process.addLostCommand({x: 5, y: 254, dir: 'W'}, 'R')
    process.addLostCommand({x: 122, y: 21, dir: 'E'}, 'F')

    expect(process.isLostCommand({x: 1, y: 2, dir: 'N'}, 'F')).to.be.equal(true)
    expect(process.isLostCommand({x: 6, y: 2, dir: 'N'}, 'F')).to.be.equal(false)
    expect(process.isLostCommand({x: 122, y: 21, dir: 'E'}, 'F')).to.be.equal(true)
  })

  it('should add lost commands to memory', () => {
    process.clearLostCommands()
    process.addLostCommand({x: 1, y: 2, dir: 'N'}, 'F')
    expect(process.getLostCommands()).to.deep.equal(['{"pos":{"x":1,"y":2,"dir":"N"},"command":"F"}'])
  })

  it('should ignore instructions which get the robot lost', () => {
    process.clearLostCommands()
    let positions = process.processMoves({
      grid: {
        x: 5,
        y: 3
      },
      robots: [
        {
          startingPoint: {
            x: 1,
            y: 1,
            dir: 'E'
          },
          moves: ['R', 'F', 'R', 'F', 'R', 'F', 'R', 'F']
        },
        {
          startingPoint: {
            x: 3,
            y: 2,
            dir: 'N'
          },
          moves: ['F', 'R', 'R', 'F', 'L', 'L', 'F', 'F', 'R', 'R', 'F', 'L', 'L']
        },
        {
          startingPoint: {
            x: 0,
            y: 3,
            dir: 'W'
          },
          moves: ['L', 'L', 'F', 'F', 'F', 'L', 'F', 'L', 'F', 'L']
        }
      ]
    })
    // Check the last move
    expect(positions[2][positions[2].length - 1]).to.deep.equal({x: 2, y: 3, dir: 'S'})
  })

  it('should format output string', () => {
    process.clearLostCommands()
    let robotPositions = [
      [
        { x: 1, y: 1, dir: 'E' },
        { x: 1, y: 1, dir: 'S' },
        { x: 1, y: 0, dir: 'S' },
        { x: 1, y: 0, dir: 'W' },
        { x: 0, y: 0, dir: 'W' },
        { x: 0, y: 0, dir: 'N' },
        { x: 0, y: 1, dir: 'N' },
        { x: 0, y: 1, dir: 'E' },
        { x: 1, y: 1, dir: 'E' }
      ],
      [
        { x: 3, y: 2, dir: 'N' },
        { x: 3, y: 3, dir: 'N' },
        { x: 3, y: 3, dir: 'E' },
        { x: 3, y: 3, dir: 'S' },
        { x: 3, y: 2, dir: 'S' },
        { x: 3, y: 2, dir: 'E' },
        { x: 3, y: 2, dir: 'N' },
        { x: 3, y: 3, dir: 'N' },
        'LOST'
      ],
      [
        { x: 0, y: 3, dir: 'W' },
        { x: 0, y: 3, dir: 'S' },
        { x: 0, y: 3, dir: 'E' },
        { x: 1, y: 3, dir: 'E' },
        { x: 2, y: 3, dir: 'E' },
        { x: 3, y: 3, dir: 'E' },
        { x: 3, y: 3, dir: 'N' },
        { x: 3, y: 3, dir: 'W' },
        { x: 2, y: 3, dir: 'W' },
        { x: 2, y: 3, dir: 'S' }
      ]
    ]

    expect(process.formatOutput(robotPositions)).to.be.equal(`1 1 E
3 3 N LOST
2 3 S`)
  })

  it('should process instruction message', () => {
    let instructionMessage = `5 3
1 1 E
RFRFRFRF

3 2 N
FRRFLLFFRRFLL

0 3 W
LLFFFLFLFL`

    expect(process.processInstructionMessage(instructionMessage)).to.be.equal(`1 1 E
3 3 N LOST
2 3 S`)
  })
})
