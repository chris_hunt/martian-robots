const winston = require('winston')
const logger = winston.createLogger({
  level: 'debug'
})

logger.add(new winston.transports.Console({
  format: winston.format.simple()
}))

module.exports = {logger: logger}
