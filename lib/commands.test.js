const expect = require('chai').expect
const commands = require('./commands')

describe('Command functions', () => {
  it('should move forward', () => {
    let newPos
    newPos = commands['F']({x: 1, y: 2, dir: 'N'})
    expect(newPos).to.deep.equal({x: 1, y: 3, dir: 'N'})

    newPos = commands['F']({x: 100, y: 22, dir: 'W'})
    expect(newPos).to.deep.equal({x: 99, y: 22, dir: 'W'})

    newPos = commands['F']({x: -1, y: 22, dir: 'E'})
    expect(newPos).to.deep.equal({x: 0, y: 22, dir: 'E'})

    newPos = commands['F']({x: 100, y: 0, dir: 'S'})
    expect(newPos).to.deep.equal({x: 100, y: -1, dir: 'S'})
  })

  it('should turn', () => {
    let newPos
    newPos = commands['R']({x: 1, y: 2, dir: 'N'})
    expect(newPos).to.deep.equal({x: 1, y: 2, dir: 'E'})

    newPos = commands['L']({x: 1, y: 2, dir: 'N'})
    expect(newPos).to.deep.equal({x: 1, y: 2, dir: 'W'})
  })
})
