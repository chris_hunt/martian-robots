const commands = require('./commands')
var lostCommands = []

function processInstructionMessage (message) {
  clearLostCommands()
  let instructions = breakdownInstructions(message)
  let robotPositions = processMoves(instructions)
  return formatOutput(robotPositions)
}

function breakdownInstructions (instructions) {
  let splitInstructions = instructions.split('\n')

  let robotCount = Math.floor(splitInstructions.length / 3)
  let splitGridSize = splitInstructions[0].split(' ')
  let robots = []
  for (let i = 0; i <= robotCount - 1; i++) {
    let splitStartingPoint = splitInstructions[1 + (3 * i)].split(' ')
    let splitMoves = splitInstructions[2 + (3 * i)].split('')
    let robot = {
      startingPoint: {
        x: parseInt(splitStartingPoint[0]),
        y: parseInt(splitStartingPoint[1]),
        dir: splitStartingPoint[2]
      },
      moves: splitMoves
    }
    robots.push(robot)
  }

  return {
    grid: {
      x: parseInt(splitGridSize[0]),
      y: parseInt(splitGridSize[1])
    },
    robots: robots
  }
}

function isOnGrid (gridSize, pos) {
  let onGrid = true
  if (
    pos.x > gridSize.x ||
    pos.y > gridSize.y ||
    pos.x < 0 ||
    pos.y < 0
  ) {
    onGrid = false
  }
  return onGrid
}

function processMoves (instructions) {
  let robotPositions = []
  for (let robot of instructions.robots) {
    let positions = [robot.startingPoint]
    for (let instruction of robot.moves) {
      let lastPos = positions[positions.length - 1]
      // Ignore command if this is a known "lost command"
      if (!isLostCommand(lastPos, instruction)) {
        let newPos = commands[instruction](lastPos)
        if (isOnGrid(instructions.grid, newPos)) {
          positions.push(newPos)
        } else {
          addLostCommand(positions[positions.length - 1], instruction)
          positions.push('LOST')
          break
        }
      }
    }
    robotPositions.push(positions)
  }
  return robotPositions
}

function clearLostCommands () {
  lostCommands = []
}

function addLostCommand (pos, command) {
  lostCommands.push(JSON.stringify({pos: pos, command: command}))
}

function getLostCommands () {
  return lostCommands
}

function isLostCommand (pos, command) {
  let lostCommandString = JSON.stringify({pos: pos, command: command})

  if (getLostCommands().includes(lostCommandString)) {
    return true
  } else {
    return false
  }
}

function formatOutput (robotPositions) {
  let output = []
  for (let robot of robotPositions) {
    if (robot[robot.length - 1] === 'LOST') {
      output.push(`${robot[robot.length - 2].x} ${robot[robot.length - 2].y} ${robot[robot.length - 2].dir} ${robot[robot.length - 1]}`)
    } else {
      output.push(`${robot[robot.length - 1].x} ${robot[robot.length - 1].y} ${robot[robot.length - 1].dir}`)
    }
  }
  return output.join('\n')
}

module.exports = {
  breakdownInstructions: breakdownInstructions,
  isOnGrid: isOnGrid,
  processMoves: processMoves,
  addLostCommand: addLostCommand,
  getLostCommands: getLostCommands,
  isLostCommand: isLostCommand,
  clearLostCommands: clearLostCommands,
  formatOutput: formatOutput,
  processInstructionMessage: processInstructionMessage
}
